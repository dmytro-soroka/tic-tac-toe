const gameArea = document.getElementById('area');
const boxes = document.getElementsByClassName('box');

const modalContent = document.getElementById('content'),
    modalBtn = document.getElementById('btn-close'),
    modalOverlay = document.getElementById('overlay'),
    modal = document.getElementById('modal-result-wrapper');

let step = 0;
let prevStep = '';

gameArea.addEventListener('click', e => {
    if (e.target.className === 'box' && prevStep !== e.target) {
        step % 2 === 0 ? e.target.innerHTML = 'X' : e.target.innerHTML = '0';
        step++;
        step < 9 ? gameResult() : calculateResult('Ничья!');
        prevStep = e.target;
    } else {
        alert('Так нельзя');
    }
});

const gameResult = () => {
    let arrResult = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    arrResult.forEach((item) => {
        if (
            boxes[item[0]].innerHTML === 'X' && boxes[item[1]].innerHTML === 'X' && boxes[item[2]].innerHTML === 'X'
        ) {
            calculateResult('Победили крестики');
        } else if (
            boxes[item[0]].innerHTML === '0' && boxes[item[1]].innerHTML === '0' && boxes[item[2]].innerHTML === '0'
        ) {
            calculateResult('Победили нолики');
        }
    })
}


const calculateResult = winner => {
    modal.style.display = 'block';
    modalContent.innerHTML = winner;
}

const closeModal = () => {
    modal.style.display = 'none';
    step = 0;
    for (let box of boxes) {
        box.innerHTML = '';
    }
}

modalBtn.addEventListener('click', closeModal);
modalOverlay.addEventListener('click', closeModal);